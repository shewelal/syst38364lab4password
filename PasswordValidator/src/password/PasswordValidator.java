package password;

/*
 * @author Alec Shewell, 991525352
 * 
 * This class validates password and it will be developed using TDD
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_NUMBER_OF_DIGITS = 2;
	
	public static boolean isValidLength(String password) {
		return password.length() >= MIN_LENGTH;
	}
	
	public static boolean hasEnoughDigits(String password) {
		int count = 0;
		char[] passwordChars = password.toCharArray();
		for (char c : passwordChars) {
			if(Character.isDigit(c)) {
				count++;
			}
		}
		if (count >= 2) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static boolean hasUppercase(String password) {
		char[] passwordChars = password.toCharArray();
		for (char c : passwordChars) {
			if(Character.isUpperCase(c)) {
				return true;
			}
		}
		return false;
	}

}
