package password;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void TestHasUpperCaseRegular() {
		assertTrue("password has no uppercases", PasswordValidator.hasUppercase("ABJDHGDGDJHS"));
	}
	@Test
	public void TestHasUpperCaseBoundaryIn() {
		assertTrue("password has no uppercase", PasswordValidator.hasUppercase("Abcdfhfjdk"));
	}
	@Test
	public void TestHasUpperCaseBoundaryOut() {
		assertFalse("password has uppercase", PasswordValidator.hasUppercase("abcdfhfjdk"));
	}
	@Test (expected=NullPointerException.class)
	public void TestHasUpperCaseException() {
		boolean test = PasswordValidator.hasUppercase(null);
		fail("password has no uppercase");
	}

}
